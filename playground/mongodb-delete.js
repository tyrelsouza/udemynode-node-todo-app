// const MongoClient = require('mongodb').MongoClient;
const {MongoClient, ObjectID} = require('mongodb');


var mongoUrl = 'mongodb://127.0.0.1:27017/TodoApp';

MongoClient.connect(mongoUrl, (err, db) => {
  if (err) {
    return console.log('Unable to connect to MongoDB Server', err);
  }

  //deleteMany
  // db.collection("Todos").deleteMany({text: "Walk the dog"}).then((result) => {
    // console.log(result);
  // })
  //deleteOne
  // db.collection("Todos").deleteOne({text: "Something to do"}).then((result) => {
  //   console.log(result);
  // })
  //findOneAndDelete
  var todo = db.collection("Todos").findOneAndDelete({text: "Something to do"}).then((result) => {
    console.log(result);
  });
  db.collection('Users').deleteMany({name: 'Tyrel'}).then((results) => {
      console.log(JSON.stringify(results, null, 2));
  })

  db.collection('Users').findOneAndDelete({_id: new ObjectID('5a1e463bf08a5672016ddd88')}).then((results) => {
      console.log(JSON.stringify(results, null, 2));
  })

  // db.close();
})
