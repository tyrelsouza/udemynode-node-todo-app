// const MongoClient = require('mongodb').MongoClient;
const {MongoClient, ObjectID} = require('mongodb');


var mongoUrl = 'mongodb://127.0.0.1:27017/TodoApp';

MongoClient.connect(mongoUrl, (err, db) => {
  if (err) {
    return console.log('Unable to connect to MongoDB Server', err);
  }
  // db.collection('Todos').findOneAndUpdate(
  //   {_id : new ObjectID("5a1e477bf08a5672016ddd94")},
  //   {
  //     $set: {
  //       completed: true
  //     }
  //   },
  //   {
  //     returnOriginal: false
  //   }
  // ).then((result) => {
  //   console.log(result);
  // })

  db.collection('Users').findOneAndUpdate(
    {_id: new ObjectID("5a1e4634f08a5672016ddd86")},
    {
      $set: {
        name: 'Tyrel',
      },
      $inc: {
        age: 1
      }
    },
    {
      returnOriginal: false
    }
  ).then((result) => {
    console.log(result);
  })

  // db.close();
})
