const expect = require('expect');
const request = require('supertest');
const {ObjectID} = require('mongodb');

const {app} = require('./../server');
const {Todo} = require('./../models/todo');

var dummy = [
  {_id: new ObjectID(), text: 'One'},
  {_id: new ObjectID(), text: 'Two', completed: true, completedAt: 333},
  {_id: new ObjectID(), text: 'Three'} ];

beforeEach((done) => {
  Todo.remove({}).then(() => {
    return Todo.insertMany(dummy);
  }).then(() => {
    done();
  });

});

describe('POST /todos', () => {
  it('should create a new todo', (done) => {
    var text = 'Test todo text';

    request(app)
      .post('/todos')
      .send({text})
      .expect(200)
      .expect((res) => {
        expect(res.body.text).toBe(text);
      })
      .end((err, res) => {
        if (err) {
          return done(err);
        }

        Todo.find().then((todos) => {
          expect(todos.length).toBe(dummy.length + 1);
          expect(todos[dummy.length].text).toBe(text);
          done();
        }).catch((e) => done(e));
      })
  });

  it('should NOT create a new todo', (done) => {
    request(app)
      .post('/todos')
      .send()
      .expect(400)
      .end((err, res) => {
        if (err) {
          return done(err);
        }
        Todo.find().then((todos) => {
          expect(todos.length).toBe(3);
          done();
        }).catch((e) => done(e));
      })
  });
});

describe("GET /todos", () => {
  it('should get all TODOS', (done) => {
    request(app)
      .get('/todos')
      .expect(200)
      .expect((res) => {
        expect(res.body.todos.length).toBe(3)

      })
      .end(done);
  })
})

describe("GET /todos/:id", () => {
  it("should get a single todo", (done) => {
    request(app)
      .get(`/todos/${dummy[0]._id.toHexString()}`)
      .expect(200)
      .expect((res) => {
        expect(res.body.todo.text).toBe(dummy[0].text)
      })
      .end(done)
  })
  it("should return a 404 if todo not found", (done) => {
    request(app)
      .get(`/todos/${new ObjectID()}`)
      .expect(404)
      .end(done)
  })
  it("should return a 404 if id is bad", (done) => {
    request(app)
      .get(`/todos/${dummy[0]._id.toHexString()}11111`)
      .expect(404)
      .end(done)
  })
});

describe("DELETE /todos/:id", () => {
  it("should delete a single todo", (done) => {
    request(app)
      .delete(`/todos/${dummy[0]._id.toHexString()}`)
      .expect(200)
      .expect((res) => {
        expect(res.body.todo.text).toBe(dummy[0].text)
      })
      .end((err, res) => {
        if(err){
          return done(err);
        }
        Todo.findById(dummy[0]._id.toHexString()).then((todo) => {
          expect(todo).toNotExist();
          done();
        }).catch((e) => done(e));
      });
  });
  it("should return a 404 if todo not found", (done) => {
    request(app)
      .delete(`/todos/${new ObjectID()}`)
      .expect(404)
      .end(done)
  });
  it("should return a 404 if id is bad", (done) => {
    request(app)
      .delete(`/todos/123`)
      .expect(404)
      .end(done)
  });
});

describe("PATCH /todos/:id", () => {
  it("should update a single todo", (done) => {
    var text = "butts";
    request(app)
      .patch(`/todos/${dummy[0]._id.toHexString()}`)
      .send({completed: true, text})
      .expect(200)
      .expect((res) => {
        var {todo} = res.body;
        expect(todo.text).toBe(text);
        expect(todo.completed).toBe(true);
        expect(todo.completedAt).toBeA('number');
      })
      .end(done)
  });

  it("should clear completedAt when todo is not completed", (done) => {
    request(app)
      .patch(`/todos/${dummy[1]._id.toHexString()}`)
      .send({completed: false})
      .expect(200)
      .expect((res) => {
        var {todo} = res.body;
        expect(todo.completedAt).toNotExist();
        expect(todo.completed).toBe(false);
      })
      .end(done)
  });
});
